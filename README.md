# m4k-mods

Mods des minetest4kids-Survival Servers.

## Vorraussetzungen
- mindestens Minetest 5.4.0
- mindestens 1 GB RAM
- git
- quilt

## Install
```
git clone https://codeberg.org/minetest4kids/m4k-mods.git
```

```
git submodule sync
```

```
git submodule init
```

```
git submodule update   
```

### patches
Die Modd 'Witt' crasht beim Blick auf einen sog. "unknown_node", hier für habe ich einen Patch erstellt der das Problem löst. Um ihn anzuwenden bitte folgenden Schritte durchführen:

```
quilt push -a
```

## Update

### patches

Vor einem update bitte immer die Patches entfernen.
```
quilt pop -a
```

Anschließend kann das Update der Mods mit folgenden Befehlen durchgeführt werden.

```
git pull
```

```
git submodule init
```

```
git submodule update
```

```
git clean -dff
```

Sofern notwendig können die Patches anschließend wieder eingespielt werden.
```
quilt push -a
```

## Thanks
[exot](https://github.com/exot/) für seine Erläuterungen mit git, quilt und vielen anderen Dingen  
[Illuna](https://illuna.rocks/) für die Tipps bzgl. der minetest.conf  
